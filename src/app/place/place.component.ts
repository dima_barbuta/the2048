import {Component, OnInit} from '@angular/core';
import { AfterViewInit } from "@angular/core";

@Component({
    selector: 'app-place',
    templateUrl: './place.component.html',
    styleUrls: ['./place.component.css'],

})
export class PlaceComponent implements OnInit {

    randomCol1: number;
    randomCol2: number;
    elem: any;
    elem1: any;


    items: any[] = new Array(16);


    constructor() {
    }

    getRandomsCols(rCol1, rCol2) {
        let elRandom1 = document.getElementById('col-' + rCol1);
        let elRandom2 = document.getElementById('col-' + rCol2);
        elRandom1.innerHTML = "2";
        elRandom2.innerHTML = "2";

    }

    ngOnInit() {}


    generateRandom(){
        this.randomCol1 = Math.floor(Math.random() * this.items.length);
        this.randomCol2 = Math.floor(Math.random() * this.items.length);

        if (this.randomCol1 == this.randomCol2){
            this.generateRandom()
        }
    }


    ngAfterViewInit() {

        this.generateRandom();


        console.log(this.randomCol1);
        console.log(this.randomCol2);
        this.getRandomsCols(this.randomCol1, this.randomCol2);


    }
}
